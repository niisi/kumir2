Build manual for openSUSE 13.1
---------------------------------

1. Required additional packages to install before build:
    - cmake
    - gcc-c++
    - libqt4-devel
    - libqca2-devel
    - libQtWebKit-devel
    - boost-devel
    - zlib-devel
    - llvm-devel
    - llvm-clang
    
2. Build and install commands
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release ../
    sudo make install # installs to /usr/local, requires root password
    
    

    