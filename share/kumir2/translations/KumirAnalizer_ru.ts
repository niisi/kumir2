<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>KumirAnalizer::Lexer</name>
    <message utf8="true">
        <location filename="../../../src/plugins/kumiranalizer/lexer.cpp" line="1334"/>
        <source>\bзнач\b|\bтаб\b</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/kumiranalizer/lexer.cpp" line="1432"/>
        <source>@testing</source>
        <translation>@тестирование</translation>
    </message>
</context>
<context>
    <name>KumirAnalizer::SyntaxAnalizer</name>
    <message>
        <location filename="../../../src/plugins/kumiranalizer/syntaxanalizer.cpp" line="122"/>
        <source>Built-it module %1</source>
        <translation>Встроенный исполнитель %1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/kumiranalizer/syntaxanalizer.cpp" line="133"/>
        <source>Use file &quot;%1&quot; as module</source>
        <translation>Использовать программу &quot;%1&quot; как исполнитель</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/kumiranalizer/syntaxanalizer.cpp" line="142"/>
        <source>Use precompiled file &quot;%1&quot; as module</source>
        <translation>Использовать код &quot;%1&quot; как исполнитель</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/kumiranalizer/syntaxanalizer.cpp" line="329"/>
        <source>Loop for variable</source>
        <translation>Цикл с изменением значения величины</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/kumiranalizer/syntaxanalizer.cpp" line="334"/>
        <source>Repeat while condition is true</source>
        <translation>Повторять пока условие истинно</translation>
    </message>
</context>
<context>
    <name>KumirAnalizer::SyntaxAnalizerPrivate</name>
    <message>
        <source>Built-it module %1</source>
        <translation type="obsolete">Встроенный исполнитель %1</translation>
    </message>
    <message>
        <source>Use file &quot;%1&quot; as module</source>
        <translation type="obsolete">Использовать программу &quot;%1&quot; как исполнитель</translation>
    </message>
    <message>
        <source>Use precompiled file &quot;%1&quot; as module</source>
        <translation type="obsolete">Использовать код &quot;%1&quot; как исполнитель</translation>
    </message>
    <message>
        <source>Loop for variable</source>
        <translation type="obsolete">Цикл с изменением значения величины</translation>
    </message>
    <message>
        <source>Repeat while condition is true</source>
        <translation type="obsolete">Повторять пока условие истинно</translation>
    </message>
</context>
</TS>
